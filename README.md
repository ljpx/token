![](icon.png)

# token

Package `token` provides a custom access token implementation signed by Ed25519.

## Usage Example

The below example is missing error handling.

```go
pub, prv, _ := ed25519.GenerateKey(rand.Reader)

token := NewToken()
token.AddClaim("userID", "1234")
token.AddScope("create")

token.Sign(prv)
tokenStr, _ := token1.Serialize()
```
