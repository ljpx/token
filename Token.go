package token

import (
	"crypto/ed25519"
	"encoding/base64"
	"encoding/json"
	"errors"
	"strings"
)

// Token represents a structured and potentially signed collection of claims and
// scopes.  A token becomes immutable after it is signed.  The only operation
// that can be called on a signed token is RemoveSignature.
//
// Methods that deal with claims and scopes intentionally do not return an
// error, but they will ultimately do nothing if the token is signed.  Use
// IsSigned to determine if the token is immutable.
type Token struct {
	claims map[string]interface{}
	scopes map[string]struct{}

	signature []byte
}

// ErrImmutableState is returned if an operation cannot complete due to the
// token being signed and therefore immutable.
var ErrImmutableState = errors.New("the token is signed and is therefore immutable")

// ErrNotSigned is returned if an operation requires that the token is signed
// before it can be called.
var ErrNotSigned = errors.New("the token either does not have a signature, or the signature on it is invalid")

// ErrInvalidState is returned when the token is in an invalid state and should
// be discarded.
var ErrInvalidState = errors.New("the token is in an invalid state and cannot be mutated or operated on")

// NewToken creates a new, empty token.
func NewToken() *Token {
	return &Token{
		claims: make(map[string]interface{}),
		scopes: make(map[string]struct{}),
	}
}

// Parse takes an existing serialized token string and reverts it back into a
// structured instance of Token.
func Parse(serializedToken string) (*Token, error) {
	spl := strings.Split(serializedToken, ".")
	if len(spl) != 2 {
		return nil, ErrNotSigned
	}

	rawB64 := spl[0]
	signatureB64 := spl[1]

	raw, err := base64.RawURLEncoding.DecodeString(rawB64)
	if err != nil {
		return nil, err
	}

	signature, err := base64.RawURLEncoding.DecodeString(signatureB64)
	if err != nil {
		return nil, err
	}

	if len(signature) != ed25519.SignatureSize {
		return nil, ErrNotSigned
	}

	t := NewToken()
	err = t.deserializeClaimsAndScopes(raw)
	if err != nil {
		return nil, err
	}

	t.signature = signature
	return t, nil
}

// AddClaim adds a claim to the token.  It will not override an existing claim.
func (t *Token) AddClaim(name string, value interface{}) {
	if t.IsSigned() {
		return
	}

	_, ok := t.claims[name]
	if ok {
		return
	}

	t.claims[name] = value
}

// SetClaim sets a claim in the token.  It will override an existing claim.
func (t *Token) SetClaim(name string, value interface{}) {
	if t.IsSigned() {
		return
	}

	t.claims[name] = value
}

// GetClaim returns the value of a claim from the token.
func (t *Token) GetClaim(name string) interface{} {
	value, _ := t.claims[name]
	return value
}

// GetStringClaim returns the value of a claim as a string.  This call will
// return an empty string if the claim does not exist or if the value is not a
// string.
func (t *Token) GetStringClaim(name string) string {
	value, ok := t.claims[name].(string)
	if !ok {
		return ""
	}

	return value
}

// HasClaim returns true if a token has the provided claim, false otherwise.
func (t *Token) HasClaim(name string) bool {
	_, ok := t.claims[name]
	return ok
}

// RemoveClaim removes a claim from the token if it is present.
func (t *Token) RemoveClaim(name string) {
	if t.IsSigned() {
		return
	}

	delete(t.claims, name)
}

// AddScope adds a scope to the token.
func (t *Token) AddScope(scope string) {
	if t.IsSigned() {
		return
	}

	t.scopes[scope] = struct{}{}
}

// HasScope returns true if a token has the provided scope, false otherwise.
func (t *Token) HasScope(scope string) bool {
	_, ok := t.scopes[scope]
	return ok
}

// RemoveScope removes a scope from the token if it is present.
func (t *Token) RemoveScope(scope string) {
	if t.IsSigned() {
		return
	}

	delete(t.scopes, scope)
}

// IsSigned returns true if the token has a signature, false otherwise.  It does
// not provide any information about the validity of the signature.
func (t *Token) IsSigned() bool {
	return t.signature != nil
}

// RemoveSignature will remove a signature on the token, if it exists.
func (t *Token) RemoveSignature() {
	t.signature = nil
}

// Sign signs the token.  If it is already signed, an error is returned.
func (t *Token) Sign(privateKey ed25519.PrivateKey) error {
	if t.IsSigned() {
		return ErrImmutableState
	}

	raw, err := t.serializeClaimsAndScopes()
	if err != nil {
		return err
	}

	t.signature = ed25519.Sign(privateKey, raw)

	return nil
}

// Verify verifies that the signature on the token matches the contents.
func (t *Token) Verify(publicKey ed25519.PublicKey) bool {
	if !t.IsSigned() {
		return false
	}

	raw, err := t.serializeClaimsAndScopes()
	if err != nil {
		return false
	}

	return ed25519.Verify(publicKey, raw, t.signature)
}

// Serialize produces the URL safe base64 string of the signed token.
func (t *Token) Serialize() (string, error) {
	if !t.IsSigned() {
		return "", ErrNotSigned
	}

	raw, err := t.serializeClaimsAndScopes()
	if err != nil {
		return "", err
	}

	rawB64 := base64.RawURLEncoding.EncodeToString(raw)
	signatureB64 := base64.RawURLEncoding.EncodeToString(t.signature)

	return rawB64 + "." + signatureB64, nil
}

func (t *Token) serializeClaimsAndScopes() ([]byte, error) {
	temp := make(map[string]interface{})
	for k, v := range t.claims {
		temp[k] = v
	}

	for k := range t.scopes {
		ssi, ok := temp["scopes"]
		if !ok {
			temp["scopes"] = make([]string, 0)
			ssi = temp["scopes"]
		}

		ss, ok := ssi.([]string)
		if !ok {
			return nil, ErrInvalidState
		}

		ss = append(ss, k)
		temp["scopes"] = ss
	}

	raw, err := json.Marshal(temp)
	if err != nil {
		return nil, err
	}

	return raw, nil
}

func (t *Token) deserializeClaimsAndScopes(raw []byte) error {
	temp := make(map[string]interface{})
	err := json.Unmarshal(raw, &temp)
	if err != nil {
		return err
	}

	ssi, ok := temp["scopes"]
	if ok {
		ss, ok := ssi.([]interface{})
		if ok {
			for _, v := range ss {
				vs, ok := v.(string)
				if !ok {
					continue
				}

				t.scopes[vs] = struct{}{}
			}
		}
	}

	for k, v := range temp {
		if k == "scopes" {
			continue
		}

		t.claims[k] = v
	}

	return nil
}
