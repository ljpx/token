package token

import (
	"crypto/ed25519"
	"crypto/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewToken(t *testing.T) {
	token := NewToken()

	assert.NotNil(t, token.claims)
	assert.NotNil(t, token.scopes)
	assert.Len(t, token.claims, 0)
	assert.Len(t, token.scopes, 0)
	assert.Nil(t, token.signature)
}

func TestClaims(t *testing.T) {
	token := NewToken()
	assert.False(t, token.HasClaim("userID"))

	token.AddClaim("userID", 1234)
	assert.True(t, token.HasClaim("userID"))
	assert.Equal(t, 1234, token.GetClaim("userID").(int))

	token.AddClaim("userID", "4321")
	assert.Equal(t, 1234, token.GetClaim("userID").(int))

	token.SetClaim("userID", "4321")
	assert.Equal(t, "4321", token.GetStringClaim("userID"))

	token.RemoveClaim("userID")
	assert.False(t, token.HasClaim("userID"))
}

func TestGetStringClaim(t *testing.T) {
	token := NewToken()

	token.AddClaim("userID", 1234)
	assert.Equal(t, "", token.GetStringClaim("userID"))
	assert.False(t, token.HasClaim("profileID"))
	assert.Equal(t, "", token.GetStringClaim("profileID"))
}

func TestScopes(t *testing.T) {
	token := NewToken()
	assert.False(t, token.HasScope("create"))

	token.AddScope("create")
	assert.True(t, token.HasScope("create"))

	token.RemoveScope("create")
	assert.False(t, token.HasScope("create"))
}

func TestIsSignedAndRemoveSignature(t *testing.T) {
	token := NewToken()
	assert.False(t, token.IsSigned())

	token.signature = make([]byte, 0)
	assert.True(t, token.IsSigned())

	token.RemoveSignature()
	assert.False(t, token.IsSigned())
}

func TestImmutableWhenSigned(t *testing.T) {
	token := NewToken()

	token.AddClaim("userID", "1234")
	token.AddScope("create")
	assert.True(t, token.HasClaim("userID"))
	assert.True(t, token.HasScope("create"))

	token.signature = make([]byte, 0)

	token.AddClaim("profileID", "4321")
	assert.False(t, token.HasClaim("profileID"))

	token.SetClaim("profileID", "4321")
	assert.False(t, token.HasClaim("profileID"))

	token.RemoveClaim("userID")
	assert.True(t, token.HasClaim("userID"))

	token.AddScope("delete")
	assert.False(t, token.HasScope("delete"))

	token.RemoveScope("create")
	assert.True(t, token.HasScope("create"))
}

func TestCannotSignIfAlreadySigned(t *testing.T) {
	token := NewToken()
	token.signature = make([]byte, 0)

	err := token.Sign(make([]byte, ed25519.PrivateKeySize))
	assert.Equal(t, ErrImmutableState, err)
}

func TestSignAndVerify(t *testing.T) {
	pub, prv, err := ed25519.GenerateKey(rand.Reader)
	assert.Nil(t, err)

	token := NewToken()
	token.AddClaim("userID", "1234")
	token.AddScope("create")

	err = token.Sign(prv)
	assert.Nil(t, err)

	assert.True(t, token.Verify(pub))
}

func TestTokenE2E(t *testing.T) {
	pub, prv, err := ed25519.GenerateKey(rand.Reader)
	assert.Nil(t, err)

	token1 := NewToken()
	token1.AddClaim("userID", "1234")
	token1.AddScope("create")

	err = token1.Sign(prv)
	assert.Nil(t, err)

	tokenStr, err := token1.Serialize()
	assert.Nil(t, err)

	token2, err := Parse(tokenStr)
	assert.Nil(t, err)

	assert.True(t, token2.Verify(pub))
}

func TestSerializeFailsIfNotSigned(t *testing.T) {
	token := NewToken()
	assert.False(t, token.IsSigned())

	_, err := token.Serialize()
	assert.Equal(t, ErrNotSigned, err)
}

func TestParseNoSignature(t *testing.T) {
	tokenStr := "aaaa"
	_, err := Parse(tokenStr)
	assert.Equal(t, ErrNotSigned, err)
}

func TestParseInvalidSignatureLength(t *testing.T) {
	tokenStr := "aaaa.bb"
	_, err := Parse(tokenStr)
	assert.Equal(t, ErrNotSigned, err)
}
